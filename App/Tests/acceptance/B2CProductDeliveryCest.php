<?php
use App\Tests\Pdf\GeneratePdf;
class B2CProductDeliveryCest
{
    public $tag = 'B2C_Delivery';
    public $filename = 'THPG_B2C_Product_Delivery';
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function B2C_One_Product(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->wait(5);
        $I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2c@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        $I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        $I->wait(3);
        $I->amOnPage("rma/");
        $I->fillField('q', '186887');
        $I->click('Suche');
        $I->wait('5');
        $I->scrollTo('#its-accordion');
        $I->click('In den Warenkorb');
        $I->wait('5');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/2');
        $I->amOnPage("/checkout/");
        $I->wait('10');
        $I->scrollTo('//*[@id="shipping-method-buttons-container"]/div/button');
        $I->dontSee('Lieferoptionen');
        $I->makeScreenshot($this->tag.'/3');
        $I->amOnPage("/");
        $I->wait('5');
        $I->fillField('q', '182947');
        $I->wait('3');
        $I->click('Suche');
        $I->wait('5');
        $I->click(' #option-label-auspraegung_3-395-item-5452');
        $I->click('#option-label-auspraegung_4-396-item-5457');
        $I->click('In den Warenkorb');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/4');
        $I->amOnPage("/checkout/");
        $I->wait('3');
        $I->scrollTo('//*[@id="shipping-method-buttons-container"]/div/button');
        $I->dontSee('Lieferoptionen');
        $I->makeScreenshot($this->tag.'/5');
        $I->wait('5');
        $pdf = new GeneratePdf();
        $pdf->Generate($this->tag,$this->filename);


    }#
}
