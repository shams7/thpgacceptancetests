<?php
use App\Tests\Pdf\GeneratePdf;
use App\Tests\Services\PriceTestMethodsCest;

class PriceTestCest

{
    public $tag = 'Price';
    public $filename = 'THPG_VAT_Price';

    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function PriceTest(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->wait(5);
        $I->click('Hinweis schließen');
        // $I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2bde@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        $I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        //$I->seeCurrentURLEquals('/');
        $I->wait(8);

        $I->amOnPage("/rma");
        $I->fillField('q', '187110');
        $I->click('Suche');
        $I->wait('5');
        $I->scrollTo('#its-accordion');
        $I->makeScreenshot($this->tag.'/2');
        $I->see('13,45');
        $I->see('Exkl');
        $I->click('In den Warenkorb');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/3');
        $I->see('13,45');
        $I->amOnPage("/checkout/cart/");
        $I->see('13,45');
        $I->wait('3');
        $I->makeScreenshot($this->tag.'/4');
        $I->click('Veränderung');
        $I->click('Abmelden');
        $I->wait('5');
        //******************************************************************
        $I->amOnPage('/');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2bdiscount10percent@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        $I->makeScreenshot($this->tag.'/5');
        $I->click('send');
       // $I->seeCurrentURLEquals('/');
        $I->wait(3);

        $I->amOnPage("rma/");
        $I->fillField('q', '187110');
        $I->click('Suche');
        $I->wait('5');
        $I->scrollTo('#its-accordion');
        $I->makeScreenshot($this->tag.'/6');
        $I->see('12,10');
        $I->see('Exkl');
        $I->click('In den Warenkorb');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/7');
        $I->see('12,10');
        $I->amOnPage("/checkout/cart/");
        $I->see('12,10');
        $I->wait('3');
        $I->makeScreenshot($this->tag.'/8');
        $I->click('Veränderung');
        $I->click('Abmelden');
        $I->wait('5');
        //******************************************************************
        $I->amOnPage('/');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2c@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        $I->makeScreenshot($this->tag.'/9');
        $I->click('send');
        //$I->seeCurrentURLEquals('/');
        $I->wait(3);
        $I->amOnPage("rma/");
        $I->fillField('q', '187110');
        $I->click('Suche');
        $I->wait('5');
        $I->scrollTo('#its-accordion');
        $I->makeScreenshot($this->tag.'/10');
        $I->see('16,00');
        $I->see('Inkl');
        $I->click('In den Warenkorb');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/11');
        $I->see('16,00');
        $I->amOnPage("/checkout/cart/");
        $I->see('16,00');
        $I->wait('3');
        $I->makeScreenshot($this->tag.'/12');
        $I->wait('3');
        $I->click('Veränderung');
        $I->click('Abmelden');
        $I->wait('5');
        $pdf = new GeneratePdf();
        $pdf->Generate($this->tag,$this->filename);




    }
}
