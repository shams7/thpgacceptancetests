<?php
include 'ShippingCostsMethodsCest.php';
use App\Tests\Pdf\GeneratePdf;

class ExpressShippingCest
{
    public $tag = 'Express';
    public $filename = 'THPG_other_product_test_express_shipping';
    public function _before(AcceptanceTester $I)
    {
    }

   public function ExpressShippingTest(AcceptanceTester $I)
   {
       $I->amOnPage('/');
       $I->wait(3);
       $I->click('Hinweis schließen');
       //$I->click('Cookies zulassen');
       $I->click('Anmelden');
       $I->fillField('login[username]', 'b2bde@example.com');
       $I->fillField('login[password]', 'Muckidee8687!');
       $I->makeScreenshot($this->tag.'/1');
       $I->click('send');
       $I->wait(2);
       $I->amOnPage("rma/");
       $I->fillField('q', '187110');
       $I->click('Suche');
       $I->wait('2');
       $I->scrollTo('#its-accordion');
       $I->click('In den Warenkorb');
       $I->wait('3');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('//*[@id="opc-shipping_method"]/div');
       $I->See('Express Versand');
       $I->makeScreenshot($this->tag.'/2');
       /*$I->amOnPage("rma/");
       $I->fillField('q', '182156');
       $I->click('Suche');
       $I->wait('2');
       $I->scrollTo('#its-accordion');
       //$I->makeScreenshot($this->tag.'/2');
       $I->wait('3');
       $I->click('In den Warenkorb');
       $I->wait('3');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('//*[@id="opc-shipping_method"]/div');
       $I->dontSee('Express Versand');
       $I->makeScreenshot($this->tag.'/3');*/
       $I->amOnPage("rma/");
       $I->fillField('q', '182947');
       $I->click('Suche');
       $I->wait('5');
       $I->scrollTo('#option-label-auspraegung_3-395-item-5452');
       $I->wait('2');
       $I->click('#option-label-auspraegung_3-395-item-5452');
       $I->click('#option-label-auspraegung_4-396-item-5457');
       $I->click('In den Warenkorb');
       $I->click('Mein Warenkorb');
       $I->wait('5');
       $I->makeScreenshot($this->tag.'/4');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('#shipping-method-buttons-container');
       $I->dontSee('Express Versand');
       $I->makeScreenshot($this->tag.'/5');
       $I->amOnPage("rma/");
       $I->wait('5');
       $I->click('Veränderung');
       $I->click('Abmelden');
       $I->wait('5');
       /******************************************************************************************/
       $I->amOnPage('/');
       $I->wait(3);
       //$I->click('Cookies zulassen');
       $I->click('Anmelden');
       $I->fillField('login[username]', 'b2beu@example.com');
       $I->fillField('login[password]', 'Muckidee8687!');
       $I->makeScreenshot($this->tag.'/6');
       $I->click('send');
       $I->wait(2);
       /*$I->amOnPage("rma/");
       $I->fillField('q', '187110');
       $I->click('Suche');
       $I->wait('2');
       $I->scrollTo('#its-accordion');
       $I->click('In den Warenkorb');
       $I->wait('3');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('//*[@id="opc-shipping_method"]/div');
       $I->dontSee('Express Versand');
       $I->makeScreenshot($this->tag.'/7');
       $I->amOnPage("rma/");
       $I->fillField('q', '182156');
       $I->click('Suche');
       $I->wait('2');
       $I->scrollTo('#its-accordion');
       //$I->makeScreenshot($this->tag.'/2');
       $I->click('In den Warenkorb');
       $I->wait('3');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('//*[@id="opc-shipping_method"]/div');
       $I->dontSee('Express Versand');
       $I->makeScreenshot($this->tag.'/8');*/
       $I->amOnPage("rma/");
       $I->fillField('q', '182947');
       $I->click('Suche');
       $I->wait('5');
       $I->scrollTo('#option-label-auspraegung_3-395-item-5452');
       $I->wait('2');
       $I->click('#option-label-auspraegung_3-395-item-5452');
       $I->click('#option-label-auspraegung_4-396-item-5457');
       $I->click('In den Warenkorb');
       $I->click('Mein Warenkorb');
       $I->wait('5');
       $I->makeScreenshot($this->tag.'/9');
       $I->amOnPage("/checkout/");
       $I->wait('10');
       $I->scrollTo('//*[@id="opc-shipping_method"]/div');
       $I->dontSee('Express Versand');
       $I->makeScreenshot($this->tag.'/10');
       $pdf = new GeneratePdf();
       $pdf->Generate($this->tag,$this->filename);
   }
}
