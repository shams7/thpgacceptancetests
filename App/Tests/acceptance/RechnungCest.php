<?php
include '../Services/RechnungMethodesCest.php';
use App\Tests\Pdf\GeneratePdf;

class RechnungCest

{
    public $tag = 'Rechnung';
    public $filename = 'THPG_Rechnung';

    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function B2B_Tests(AcceptanceTester $I)
    {
        /*$I->amOnPage('/');
        $I->wait(3);
        $I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2bkeyaccount@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        //$I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        $I->wait(2);
        $I->amOnPage("rma/");
        $I->fillField('q', '182156');
        $I->click('Suche');
        $I->wait('2');
        $I->scrollTo('#its-accordion');
        //$I->makeScreenshot($this->tag.'/2');
        $I->click('In den Warenkorb');
        $I->wait('3');
        $I->amOnPage("/checkout/cart/");
        $I->wait('3');
        //$I->makeScreenshot($this->tag.'/3');
        $I->amOnPage("/checkout/");
        $I->wait('10');
        $I->click('Weiter');
        $I->wait('10');
        $I->See('Rechnung');
       // $I->makeScreenshot($this->tag.'/4');
        $I->amOnPage('/');
        $I->wait('3');
        $I->click('Veränderung');
        $I->click('Abmelden');*/
        /**********************************************************************************/
        /*$I->amOnPage('/');
        $I->wait(3);
        //$I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2boneoreder@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        //$I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        $I->wait(2);
        $I->amOnPage("rma/");
        $I->fillField('q', '182156');
        $I->click('Suche');
        $I->wait('2');
        $I->scrollTo('#its-accordion');
        //$I->makeScreenshot($this->tag.'/2');
        $I->click('In den Warenkorb');
        $I->wait('3');
        $I->amOnPage("/checkout/cart/");
        $I->wait('3');
        //$I->makeScreenshot($this->tag.'/3');
        $I->amOnPage("/checkout/");
        $I->wait('10');
        $I->click('Weiter');
        $I->wait('10');
        $I->See('Rechnung');
        // $I->makeScreenshot($this->tag.'/4');
        $I->amOnPage('/');
        $I->wait('3');
        $I->click('Veränderung');
        $I->click('Abmelden');*/
        /**********************************************************************************/
        $I->amOnPage('/');
        $I->wait(3);
        //$I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2cbadrating@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        //$I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        $I->wait(2);
        $I->amOnPage("rma/");
        $I->fillField('q', '182156');
        $I->click('Suche');
        $I->wait('2');
        $I->scrollTo('#its-accordion');
        //$I->makeScreenshot($this->tag.'/2');
        $I->click('In den Warenkorb');
        $I->wait('3');
        $I->amOnPage("/checkout/cart/");
        $I->wait('3');
        //$I->makeScreenshot($this->tag.'/3');
        $I->amOnPage("/checkout/");
        $I->wait('10');
        $I->scrollTo('//*[@id="opc-shipping_method"]/div');
        $I->click('Weiter');
        $I->wait('10');
        $I->See('Überweisung');
        // $I->makeScreenshot($this->tag.'/4');
        $I->amOnPage('/');
        $I->wait('3');
        $I->click('Veränderung');
        $I->click('Abmelden');


        /**********************************************************************************/
//        $I->amOnPage('/');
//        $I->wait(3);
//        //$I->click('Cookies zulassen');
//        $I->click('Anmelden');
//        $I->fillField('login[username]', 'b2bkeyaccounteur@example.com');
//        $I->fillField('login[password]', 'Muckidee8687!');
//        //$I->makeScreenshot($this->tag.'/1');
//        $I->click('send');
//        $I->wait(2);
//        $I->amOnPage("rma/");
//        $I->fillField('q', '182156');
//        $I->click('Suche');
//        $I->wait('2');
//        $I->scrollTo('#its-accordion');
//        //$I->makeScreenshot($this->tag.'/2');
//        $I->click('In den Warenkorb');
//        $I->wait('3');
//        $I->amOnPage("/checkout/cart/");
//        $I->wait('3');
//        //$I->makeScreenshot($this->tag.'/3');
//        $I->amOnPage("/checkout/");
//        $I->wait('10');
//        $I->scrollTo('//*[@id="opc-shipping_method"]/div');
//        $I->click('Weiter');
//        $I->wait('10');
//        $I->dontSee('Rechnung');
//        // $I->makeScreenshot($this->tag.'/4');
//        $I->amOnPage('/');
//        $I->wait('3');
//        $I->click('Veränderung');
//        $I->click('Abmelden');
//        /**********************************************************************************/
//        $I->amOnPage('/');
//        $I->wait(3);
//        //$I->click('Cookies zulassen');
//        $I->click('Anmelden');
//        $I->fillField('login[username]', 'b2bzeroorders@example.com');
//        $I->fillField('login[password]', 'Muckidee8687!');
//        //$I->makeScreenshot($this->tag.'/1');
//        $I->click('send');
//        $I->wait(2);
//        $I->amOnPage("rma/");
//        $I->fillField('q', '182156');
//        $I->click('Suche');
//        $I->wait('2');
//        $I->scrollTo('#its-accordion');
//        //$I->makeScreenshot($this->tag.'/2');
//        $I->click('In den Warenkorb');
//        $I->wait('3');
//        $I->amOnPage("/checkout/cart/");
//        $I->wait('3');
//        //$I->makeScreenshot($this->tag.'/3');
//        $I->amOnPage("/checkout/");
//        $I->wait('10');
//        $I->scrollTo('//*[@id="opc-shipping_method"]/div');
//        $I->click('Weiter');
//        $I->wait('10');
//        $I->dontSee('Rechnung');
//        // $I->makeScreenshot($this->tag.'/4');
//        $I->amOnPage('/');
//        $I->wait('3');
//        $I->click('Veränderung');
//        $I->click('Abmelden');
//        /**********************************************************************************/
//        $I->amOnPage('/');
//        $I->wait(3);
//       // $I->click('Cookies zulassen');
//        $I->click('Anmelden');
//        $I->fillField('login[username]', 'b2bkeyaccount@example.com');
//        $I->fillField('login[password]', 'Muckidee8687!');
//        //$I->makeScreenshot($this->tag.'/1');
//        $I->click('send');
//        $I->wait(2);
//        $I->amOnPage("rma/");
//        $I->fillField('q', '182156');
//        $I->click('Suche');
//        $I->wait('2');
//        $I->scrollTo('#its-accordion');
//        //$I->makeScreenshot($this->tag.'/2');
//        $I->click('In den Warenkorb');
//        $I->wait('3');
//        $I->amOnPage("/checkout/cart/");
//        $I->wait('3');
//        $I->fillField('cart[1640][qty]', '5');
//        $I->click('update_cart_action');
//        $I->wait('3');
//        //$I->makeScreenshot($this->tag.'/3');
//        $I->amOnPage("/checkout/");
//        $I->wait('10');
//        $I->scrollTo('//*[@id="opc-shipping_method"]/div');
//        $I->click('Weiter');
//        $I->wait('10');
//        $I->dontSee('Rechnung');
//        // $I->makeScreenshot($this->tag.'/4');
//        $I->amOnPage('/');
//        $I->wait('3');
//        $I->click('Veränderung');
//        $I->click('Abmelden');
//        /**********************************************************************************/
//        $I->amOnPage('/');
//        $I->wait(3);
//        //$I->click('Cookies zulassen');
//        $I->click('Anmelden');
//        $I->fillField('login[username]', 'b2boneoreder@example.com');
//        $I->fillField('login[password]', 'Muckidee8687!');
//        //$I->makeScreenshot($this->tag.'/1');
//        $I->click('send');
//        $I->wait(2);
//        $I->amOnPage("rma/");
//        $I->fillField('q', '182156');
//        $I->click('Suche');
//        $I->wait('2');
//        $I->scrollTo('#its-accordion');
//        //$I->makeScreenshot($this->tag.'/2');
//        $I->click('In den Warenkorb');
//        $I->wait('3');
//        $I->amOnPage("/checkout/cart/");
//        $I->wait('3');
//        $I->fillField('cart[1640][qty]', '5');
//        $I->click('update_cart_action');
//        $I->wait('3');
//        //$I->makeScreenshot($this->tag.'/3');
//        $I->amOnPage("/checkout/");
//        $I->wait('10');
//        $I->scrollTo('//*[@id="opc-shipping_method"]/div');
//        $I->click('Weiter');
//        $I->wait('10');
//        $I->dontSee('Rechnung');
//        // $I->makeScreenshot($this->tag.'/4');
//        $I->amOnPage('/');
//        $I->wait('3');
//        $I->click('Veränderung');
//        $I->click('Abmelden');
//
//


    }
}
