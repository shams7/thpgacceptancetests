<?php

class USA_customerCest
{
    public $tag = 'usa_customer';
    public $filename='Thpg_USA_Customer_Shipping_Costs';
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $shipping_cost = '100,00 €';

        $I->amOnPage('/');
        $I->wait('3');
        $I->makeScreenshot($this->tag.'/1');
        $I->seeLink('Anmelden');
        $I->click('Anmelden');
        $I->wait('1');
        $I->click('//*[@id="btn-cookie-allow"]');
        $I->wait('2');
        $I->fillField('#email','usacustomer@example.com');
        $I->fillField('#pass','Muckidee8687!');
        $I->click('#send2');
        $I->wait('5');
        $I->amOnPage('/rma');
        $I->fillField('#search', '182553');
        $I->click('Suche');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/2');
        $I->scrollTo('//*[@id="maincontent"]/div[2]/div/div[1]/div[4]');
        $I->click('#product-addtocart-button');
        $I->amOnPage('/checkout/cart/');
        $I->wait('10');
        $I->see($shipping_cost);
        $I->makeScreenshot($this->tag.'/3');
        $I->fillField('//*[@id="shopping-cart-table"]/tbody/tr[1]/td[3]/div/div/label/input', '16');
        $I->click('update_cart_action');
        $I->wait('8');
        $I->see($shipping_cost);
        $I->makeScreenshot($this->tag.'/4a');
        $I->amOnPage('/schalterserien/porzellan-weiss/');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[1]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[1]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[2]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[2]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[3]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[3]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->amOnPage('/checkout/cart/');
        $I->wait('5');

        $I->makeScreenshot($this->tag.'/5');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/6');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->fillField('//*[@id="shopping-cart-table"]/tbody/tr[1]/td[3]/div/div/label/input', '1');
        $I->click('update_cart_action');
        $I->wait('8');


        $I->makeScreenshot($this->tag.'/7');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/8');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->click('//*[@id="shopping-cart-table"]/tbody[2]/tr[2]/td/div/a[3]');
        $I->click('//*[@id="shopping-cart-table"]/tbody[2]/tr[2]/td/div/a[3]');
        $I->wait('3');

        $I->makeScreenshot($this->tag.'/9');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/10');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->fillField('//*[@id="shopping-cart-table"]/tbody/tr[1]/td[3]/div/div/label/input', '16');
        $I->click('update_cart_action');
        $I->wait('10');

        $I->makeScreenshot($this->tag.'/11');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/12');

        $I->amOnPage('/schalterserien/porzellan-weiss/');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[1]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[1]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[2]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[2]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->moveMouseOver('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[3]/div/div[3]/div/div/form/button');
        $I->click('//*[@id="maincontent"]/div[3]/div[1]/div[3]/ol/li[3]/div/div[3]/div/div/form/button');
        $I->wait('2');
        $I->amOnPage('/checkout/cart/');
        $I->wait('5');

        $I->makeScreenshot($this->tag.'/13');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/14');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->fillField('//*[@id="shopping-cart-table"]/tbody/tr[1]/td[3]/div/div/label/input', '1');
        $I->click('update_cart_action');
        $I->wait('10');

        $I->makeScreenshot($this->tag.'/15');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/16');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->click('//*[@id="shopping-cart-table"]/tbody[1]/tr[2]/td/div/a[3]');
        $I->wait('5');

        $I->makeScreenshot($this->tag.'/17');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/18');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->fillField('//*[@id="shopping-cart-table"]/tbody/tr[1]/td[3]/div/div/label/input', '10');
        $I->click('update_cart_action');
        $I->wait('5');

        $I->makeScreenshot($this->tag.'/19');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/20');
        $I->wait('5');
        $I->amOnPage('/checkout/cart/');
        $I->wait('3');
        $I->click('//*[@id="shopping-cart-table"]/tbody[2]/tr[2]/td/div/a[3]');
        $I->wait('3');
        $I->click('//*[@id="shopping-cart-table"]/tbody[2]/tr[2]/td/div/a[3]');
        $I->wait('5');

        $I->makeScreenshot($this->tag.'/21');
        $I->amOnPage('/checkout/');
        $I->waitForElement('//*[@id="opc-shipping_method"]/div/div[1]');

        $I->makeScreenshot($this->tag.'/22');
        $pdf = new \App\Tests\Pdf\GeneratePdf();
        $pdf->Generate($this->tag,$this->filename);
    }
}
