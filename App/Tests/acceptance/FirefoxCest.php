<?php


class FirefoxCest
{
    public $tag = 'Chrome';
    public $filename = 'THPG_Chrome';
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function Firfox(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->wait(5);
        $I->click('Hinweis schließen');
        // $I->click('Cookies zulassen');
        $I->click('Anmelden');
        $I->fillField('login[username]', 'b2bde@example.com');
        $I->fillField('login[password]', 'Muckidee8687!');
        $I->makeScreenshot($this->tag.'/1');
        $I->click('send');
        //$I->seeCurrentURLEquals('/');
        $I->wait(8);

        $I->amOnPage("/rma");
        $I->fillField('q', '187110');
        $I->click('Suche');
        $I->wait('5');
        $I->scrollTo('#its-accordion');
        $I->makeScreenshot($this->tag.'/2');
        $I->see('13,45');
        $I->see('Exkl');
        $I->click('In den Warenkorb');
        $I->click('Mein Warenkorb');
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/3');
        $I->amOnPage("/checkout/");
        $I->wait('5');
        $I->makeScreenshot($this->tag.'/4');
        $I->click('Weiter');
        $I->waitForElement('#ppplus iframe');
        $I->switchToIFrame('#ppplus iframe');
        $I->waitForElement(' //*[@id="Vorkasse per Überweisung"]');


        $I->wait('3');
        $I->click('//*[@id="Vorkasse per Überweisung"]');
        //$I->waitForElement('#place-ppp-order');
        //$I->click('#place-ppp-order');

        $I->wait('3');
        $I->makeScreenshot($this->tag.'/5');
        $I->switchToIFrame();
        //$I->scrollTo('//*[@id="checkout-payment-method-load"]/div/div/div[2]/div/div[3]/div');
         $I->wait('3');
        $I->click('Zahlungspflichtig bestellen');
        $I->wait('10');
        $I->makeScreenshot($this->tag.'/6');
        $pdf = new \App\Tests\Pdf\GeneratePdf();
        $pdf->Generate($this->tag,$this->filename);
        //$I->click('#place-ppp-order');


    }
}
